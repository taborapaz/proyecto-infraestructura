provider "aws" {
 region = "us-east-1"
 access_key = "${var.acceskey}"
 secret_key = "${var.secretkey}"
}

resource "aws_instance" "web" {
 ami  = "ami-04b9e92b5572fa0d1"
 instance_type = "t2.micro"
 key_name = "${var.llavename}"
 security_groups = ["${aws_security_group.allow_tls.name}"]

 tags = {
  Name = "Deyvi"
 }
}

resource "aws_default_vpc" "default"{
 tags = {
  Name = "Default VPC"
 }
}

resource "aws_security_group" "allow_tls"{
 name = "security_group_proyecto"
 description = "security_group_ssh_proyecto"
 vpc_id = "${aws_default_vpc.default.id}"

 ingress {
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
 }
 
 ingress {
  from_port = 1111
  to_port = 1111
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
 }

 egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
 }
}
